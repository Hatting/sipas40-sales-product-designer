import { SipasSalesProductDesignerPage } from './app.po';

describe('sipas-sales-product-designer App', () => {
  let page: SipasSalesProductDesignerPage;

  beforeEach(() => {
    page = new SipasSalesProductDesignerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
