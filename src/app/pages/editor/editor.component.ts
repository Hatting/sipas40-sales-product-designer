import { Component, OnInit } from '@angular/core';
import { DocumentsService } from '../../services/';
import {SalesProduct,SalesProductUiProperties} from '../../model/salesproduct';



@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  documentSub: any;
  name: string;
  value: any;
  salesProduct: SalesProduct;

  constructor(private ds: DocumentsService) {
    console.log(ds);
    this.documentSub = this.ds.document.subscribe(
      docu => this.salesProduct = JSON.parse(JSON.stringify(docu)));
     console.log(  this.salesProduct.generalGroups);

   };

  ngOnInit() {
  }

  ngOnDestroy() {
    this.documentSub.unsubscribe();
  }

}
