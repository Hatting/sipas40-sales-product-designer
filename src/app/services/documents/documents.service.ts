import { Injectable } from '@angular/core';
import {Observable,  BehaviorSubject } from 'rxjs';
import {MyClass} from '../../model/myclass';

@Injectable()
export class DocumentsService {

document = new BehaviorSubject<any>({});

  constructor() {
    this.document.next(

{
  'salesProduct': {
    'salesProductSystemProperties': {
      'schema': 'SalesProductSchema_v2',
      'schemaVersion': '2.0',
      'salesProduct': 'SP001 ',
      'GUID': 'SP001-A1A1A1',
      'salesCustomer': 'SC001-B1B1B1',
      'backendProducts': [
        'BP001',
        'BP002'
      ],
      'backendPolicies': [
        {
          'backendProduct': 'BP001',
          'backendPolicy': '17000-0000001-10'
        },
        {
          'backendProduct': 'BP002',
          'backendPolicy': '17000-0000002-10'
        }
      ]
    },
    'salesProductUiProperties': {
      'label': 'Sales Product SP001'
    },
    'generalGroups': [
      {
        'generalGroupSystemProperties': {
          'group': 'GG001',
          'GUID': 'GG001-C2C1C1'
        },
        'generalGroupUiProperties': {
          'order': 1,
          'label': 'General Group GG001'
        },
        'fields': [
          {
            'fieldSystemProperties': {
              'field': 'SP001-GG001-F00001',
              'GUID': 'A1A1A1',
              'backendProductFields': [
                'BP001-BPF001',
                'BP002-BPF001'
              ]
            },
            'fieldUiProperties': {
              'page': 1,
              'order': 1,
              'label': 'Field F00001',
              'mandatory': true,
              'default': 'Def value',
              'dataType': 'STRING',
              'format': '[A-Z0-9]',
              'readonly': false,
              'visible': true,
              'lov': []
            },
            'value': {
              'dataType': 'STRING',
              'format': '',
              'value': 'value F00001'
            }
          },
          {
            'fieldSystemProperties': {
              'field': 'SP001-GG001-F00004',
              'GUID': 'A2A2A2',
              'backendProductFields': [
                'BP001-OI001-C001-f001',
                'BP002-OI001-C001-f001'
              ]
            },
            'fieldUiProperties': {
              'page': 1,
              'order': 2,
              'label': 'Field F00004',
              'mandatory': true,
              'default': '',
              'dataType': 'NUMBER',
              'format': '10.2',
              'readonly': false,
              'visible': true,
              'lov': []
            },
            'value': {
              'dataType': 'NUMBER',
              'format': '10.2',
              'value': '5000.00'
            }
          }
        ]
      }
    ],
    'objectGroups': [
      [
        {
          'objectGroupSystemProperties': {
            'group': 'SP001-OG001',
            'GUID': 'A3A4A4',
            'backendProductObject': 'BP001-OI001',
            'max': 1,
            'min': 1,
            'backendPolicyObject': [
              '17000-0000001-10-111'
            ]
          },
          'objectGroupUiProperties': {
            'order': 1,
            'label': 'BP001 Object details'
          },
          'fields': [
            {
              'fieldSystemProperties': {
                'field': 'SP001-OG001-F00001',
                'GUID': 'A2A3A4',
                'backendProductFields': [
                  'BP001-OI001-BPF001'
                ]
              },
              'fieldUiProperties': {
                'page': 1,
                'order': 1,
                'label': 'Obj field ',
                'default': 'sales package default',
                'visible': true,
                'lov': [
                  {
                    'orderNum': 1,
                    'label': 'Label 1',
                    'value': {
                      'dataType': 'STRING',
                      'format': 'format 1',
                      'value': 'value 1'
                    }
                  },
                  {
                    'orderNum': 2,
                    'label': 'Label 2',
                    'value': {
                      'dataType': 'STRING',
                      'format': 'format 2',
                      'value': 'value 2'
                    }
                  }
                ],
                'mandatory': true,
                'dataType': 'STRING',
                'format': '',
                'readonly': true
              },
              'value': {
                'dataType': 'STRING',
                'format': '',
                'value': 'value F00002'
              }
            }
          ],
          'coverGroups': [
            {
              'coverGroupSystemProperties': {
                'group': 'SP001-OG001-CG001',
                'GUID': 'A4A4A4',
                'backendProductCovers': [
                  'BP001-OI001-C001'
                ],
                'backendPolicyCovers': [
                  '15000'
                ]
              },
              'coverGroupUiProperties': {
                'order': 1,
                'label': 'cover 1',
                'mandatory': true,
                'default': true
              },
              'fields': [
                {
                  'fieldSystemProperties': {
                    'field': 'SP001-OG001-CG001-F00001',
                    'GUID': 'A6A6A6',
                    'backendProductFields': [
                      'BP001-OI001-C001-F001'
                    ]
                  },
                  'fieldUiProperties': {
                    'page': 1,
                    'order': 1,
                    'label': 'label f00001',
                    'default': '',
                    'visible': true,
                    'lov': [],
                    'required': true,
                    'mandatory': true,
                    'dataType': 'NUMBER',
                    'format': '',
                    'readonly': false
                  },
                  'value': {
                    'dataType': 'NUMBER',
                    'format': '10.2',
                    'value': '500.00'
                  }
                }
              ],
              'active': true,
              'premium': 150.00
            }
          ]
        }
      ],
      [
        {
          'objectGroupSystemProperties': {
            'group': 'SP001-OG002',
            'GUID': 'A8A9A9',
            'backendProductObject': 'BP002-OI001',
            'max': 1,
            'min': 1,
            'backendPolicyObject': [
              '17000-0000002-10-112'
            ]
          },
          'objectGroupUiProperties': {
            'order': 2,
            'label': 'BP002 Object details'
          },
          'fields': [
            {
              'fieldSystemProperties': {
                'field': 'SP001-OG002-F00004',
                'GUID': 'A7A7A7',
                'backendProductFields': [
                  'BP002-OI001-BPF001'
                ]
              },
              'fieldUiProperties': {
                'page': 1,
                'order': 1,
                'label': 'Obj 2 field ',
                'default': 'sales package default',
                'visible': true,
                'lov': [],
                'mandatory': true,
                'dataType': 'STRING',
                'format': '',
                'readonly': true,
                'semanticType': {
                  'method': 'search',
                  'map': [
                    {
                      'searchParam': 'search',
                      'salesField': 'SP001-GG002-F00004'
                    },
                    {
                      'searchParam': 'CarMake',
                      'salesField': 'SP001-GG002-F00005'
                    },
                    {
                      'searchParam': 'CarType',
                      'salesField': 'SP001-GG002-F00006'
                    },
                    {
                      'searchParam': 'CarYear',
                      'salesField': 'SP001-GG002-F00007'
                    }
                  ],
                  'serviceReference': 'thisServiceReference'
                }
              },
              'value': {
                'dataType': 'STRING',
                'format': '',
                'value': 'value F00005'
              }
            },
            {
              'fieldSystemProperties': {
                'field': 'SP001-OG002-F00005',
                'GUID': 'A7A7A6',
                'backendProductFields': [
                  'BP002-OI001-BPF001'
                ]
              },
              'fieldUiProperties': {
                'page': 1,
                'order': 2,
                'label': 'Obj 3 field ',
                'default': '',
                'visible': true,
                'lov': [],
                'mandatory': true,
                'dataType': 'STRING',
                'format': '',
                'readonly': true,
                'semanticType': {
                  'method': 'fill'
                }
              },
              'value': {
                'dataType': 'STRING',
                'format': '',
                'value': 'value F00005'
              }
            },
            {
              'fieldSystemProperties': {
                'field': 'SP001-OG002-F00006',
                'GUID': 'A7A7A1',
                'backendProductFields': [
                  'BP002-OI001-BPF001'
                ]
              },
              'fieldUiProperties': {
                'page': 1,
                'order': 3,
                'label': 'Obj 4 field ',
                'default': '',
                'visible': true,
                'lov': [],
                'mandatory': true,
                'dataType': 'STRING',
                'format': '',
                'readonly': true,
                'semanticType': {
                  'method': 'fill'
                }
              },
              'value': {
                'dataType': 'STRING',
                'format': '',
                'value': 'value F6'
              }
            },
            {
              'fieldSystemProperties': {
                'field': 'SP001-OG002-F00007',
                'GUID': 'A7A7A2',
                'backendProductFields': [
                  'BP002-OI001-BPF001'
                ]
              },
              'fieldUiProperties': {
                'page': 1,
                'order': 4,
                'label': 'Obj 3 field ',
                'default': '',
                'visible': true,
                'lov': [],
                'mandatory': true,
                'dataType': 'STRING',
                'format': '',
                'readonly': true,
                'semanticType': {
                  'method': 'fill'
                }
              },
              'value': {
                'dataType': 'STRING',
                'format': '',
                'value': 'value F7'
              }
            }
          ],
          'coverGroups': [
            {
              'coverGroupSystemProperties': {
                'group': 'CG002',
                'GUID': 'CG002-A1A1A1',
                'backendProductCovers': [
                  'BP002-OI001-C001'
                ],
                'backendPolicyCovers': [
                  '15002'
                ]
              },
              'coverGroupUiProperties': {
                'order': 1,
                'label': 'cover 2',
                'mandatory': true,
                'default': true
              },
              'fields': [
              ],
              'active': true,
              'premium': 150.00
            }
          ]
        }
      ]
    ],
    'staticGroup': {
      'staticGroupSystemProperties': {
        'group': 'SG001',
        'GUID': 'SG001-A1A1A1'
      },
      'fields': [
        {
          'fieldSystemProperties': {
            'field': 'f00007',
            'GUID': 'f00007-A1A1A1',
            'backendProductFields': [
              'BP002-BPF002'
            ]
          },
          'fieldUiProperties': {
            'label': 'label f00006',
            'default': '',
            'visible': true,
            'lov': [],
            'required': true,
            'mandatory': true,
            'dataType': 'STRING',
            'format': '',
            'readonly': false
          },
          'value': {
            'dataType': 'NUMBER',
            'format': '10.2',
            'value': '500.00'
          }
        }
      ]
    }
  }
}


);
  }



}
