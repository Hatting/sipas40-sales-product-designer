import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { NotFoundComponent } from './pages';
import { HomepageComponent } from './pages';
import { RoutingModule } from './routing/routing.module';
import { DocumentsService } from './services';
import { EditorComponent } from './pages/editor/editor.component';
import { InspectorComponent } from './components/editor/inspector/inspector.component';
import { WorkareaComponent } from './components/editor/workarea/workarea.component';
import { PaletteComponent } from './components/editor/palette/palette.component';
import { SalesproductsComponent } from './components/homepage/salesproducts/salesproducts.component';
import { SaleproductviewerComponent } from './components/homepage/saleproductviewer/saleproductviewer.component';
@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomepageComponent,
    EditorComponent,
    InspectorComponent,
    WorkareaComponent,
    PaletteComponent,
    SalesproductsComponent,
    SaleproductviewerComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RoutingModule,
  ],
  providers: [DocumentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
