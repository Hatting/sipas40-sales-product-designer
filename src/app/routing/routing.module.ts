import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
	HomepageComponent,
	NotFoundComponent,
	EditorComponent
} from '../pages';

const routes: Routes = [
	{ path: '', redirectTo: '/editor', pathMatch: 'full' },
	{ path: 'homepage',  component: HomepageComponent },
	{ path: 'editor' , component: EditorComponent},
 	{ path: '404', component: NotFoundComponent},
  	{ path: '**', redirectTo: '/404' }
];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})

export class RoutingModule {}
