import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaleproductviewerComponent } from './saleproductviewer.component';

describe('SaleproductviewerComponent', () => {
  let component: SaleproductviewerComponent;
  let fixture: ComponentFixture<SaleproductviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaleproductviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaleproductviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
