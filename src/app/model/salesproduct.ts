// Generated using typescript-generator version 1.25.322 on 2017-05-11 14:52:33.

export interface SalesProduct {
    salesProductSystemProperties: SalesProductSystemProperties;
    salesProductUiProperties: SalesProductUiProperties;
    generalGroups: GeneralGroup[];
    objectGroups: ObjectGroup[][];
    staticGroup: StaticGroup;
}

export class SalesProductSystemProperties {
    schema: string;
    schemaVersion: string;
    salesProduct: string;
    GUID: string;
    salesCustomer: string;
    backendProducts: string[];
    backendPolicies: BackendPolicy[];
    opt: Opt[];
}

export class SalesProductUiProperties {
    label: string;
    opt: Opt[];
}

export class GeneralGroup {
    generalGroupSystemProperties: GeneralGroupSystemProperties;
    generalGroupUiProperties: GeneralGroupUiProperties;
    fields: Field[];
}

export class ObjectGroup {
    objectGroupSystemProperties: ObjectGroupSystemProperties;
    objectGroupUiProperties: ObjectGroupUiProperties;
    fields: Field[];
    coverGroups: CoverGroup[];
}

export class StaticGroup {
    staticGroupSystemProperties: StaticGroupSystemProperties;
    fields: Field[];
}

export class BackendPolicy {
    backendProduct: string;
    backendPolicy: string;
}

export class Opt {
    name: string;
    value: Value;
}

export class GeneralGroupSystemProperties {
    group: string;
    GUID: string;
    opt: Opt[];
}

export class GeneralGroupUiProperties {
    order: number;
    label: string;
    opt: Opt[];
}

export class Field {
    fieldSystemProperties: FieldSystemProperties;
    fieldUiProperties: FieldUiProperties;
    value: Value;
}

export class ObjectGroupSystemProperties {
    group: string;
    GUID: string;
    backendProductObject: string;
    backendPolicyObject: string[];
    max: number;
    min: number;
    opt: Opt[];
}

export class ObjectGroupUiProperties {
    order: number;
    label: string;
    opt: Opt[];
}

export class CoverGroup {
    coverGroupSystemProperties: CoverGroupSystemProperties;
    coverGroupUiProperties: CoverGroupUiProperties;
    fields: Field[];
    active: boolean;
    premium: number;
}

export class StaticGroupSystemProperties {
    group: string;
    GUID: string;
    opt: Opt[];
}

export class Value {
    dataType: string;
    format: string;
    value: string;
}

export class FieldSystemProperties {
    field: string;
    GUID: string;
    backendProductFields: string[];
    opt: Opt[];
}

export class FieldUiProperties {
    page: number;
    order: number;
    label: string;
    default: string;
    visible: boolean;
    lov: Lov[];
    mandatory: boolean;
    dataType: DataType;
    format: string;
    readonly: boolean;
    semanticType: SemanticType;
    opt: Opt[];
}

export class CoverGroupSystemProperties {
    group: string;
    GUID: string;
    backendProductCovers: string[];
    backendPolicyCovers: string[];
    opt: Opt[];
}

export class CoverGroupUiProperties {
    order: number;
    label: string;
    mandatory: boolean;
    default: boolean;
    opt: Opt[];
}

export class Lov {
    orderNum: number;
    value: Value;
    label: string;
}

export class SemanticType {
    method: string;
    map: Map[];
    serviceReference: string;
}

export class Map {
    searchParam: string;
    salesField: string;
}

export type DataType = "STRING" | "NUMBER" | "DATE";
